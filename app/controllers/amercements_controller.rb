class AmercementsController < ApplicationController
  before_action :set_amercement, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!, except: [:index, :show]

  # GET /amercements
  # GET /amercements.json
  def index
    @amercements = Amercement.all
  end

  # GET /amercements/1
  # GET /amercements/1.json
  def show
  end

  # GET /amercements/new
  def new
    @amercement = Amercement.new
  end

  # GET /amercements/1/edit
  def edit
  end

  # POST /amercements
  # POST /amercements.json
  def create
    @amercement = Amercement.new(amercement_params)

    respond_to do |format|
      if @amercement.save
        format.html { redirect_to @amercement, notice: 'Amercement was successfully created.' }
        format.json { render :show, status: :created, location: @amercement }
      else
        format.html { render :new }
        format.json { render json: @amercement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /amercements/1
  # PATCH/PUT /amercements/1.json
  def update
    respond_to do |format|
      if @amercement.update(amercement_params)
        format.html { redirect_to @amercement, notice: 'Amercement was successfully updated.' }
        format.json { render :show, status: :ok, location: @amercement }
      else
        format.html { render :edit }
        format.json { render json: @amercement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /amercements/1
  # DELETE /amercements/1.json
  def destroy
    @amercement.destroy
    respond_to do |format|
      format.html { redirect_to amercements_url, notice: 'Amercement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_amercement
      @amercement = Amercement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def amercement_params
      params.require(:amercement).permit(:id_loan, :price)
    end
end
