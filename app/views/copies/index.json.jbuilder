json.array!(@copies) do |copy|
  json.extract! copy, :id, :id_book, :copyStatus
  json.url copy_url(copy, format: :json)
end
