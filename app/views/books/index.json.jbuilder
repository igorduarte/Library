json.array!(@books) do |book|
  json.extract! book, :id, :name, :id_publisher, :id_author, :synopsis, :pages, :releaseYear, :language
  json.url book_url(book, format: :json)
end
