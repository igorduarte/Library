json.array!(@authors) do |author|
  json.extract! author, :id, :name, :site
  json.url author_url(author, format: :json)
end
