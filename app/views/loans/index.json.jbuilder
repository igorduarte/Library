json.array!(@loans) do |loan|
  json.extract! loan, :id, :id_copy, :id_client, :id_attendant, :startDate, :expectedDate, :deliveryDate, :loanStatus
  json.url loan_url(loan, format: :json)
end
