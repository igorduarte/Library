json.array!(@evaluations) do |evaluation|
  json.extract! evaluation, :id, :id_book, :id_client, :text, :grade
  json.url evaluation_url(evaluation, format: :json)
end
