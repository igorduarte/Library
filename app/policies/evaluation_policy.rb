class EvaluationPolicy < ApplicationPolicy

	def index?
		user.admin? || user.attendant?
	end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
