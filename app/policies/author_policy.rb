class AuthorPolicy < ApplicationPolicy

	def index?
		user.admin? || user.librarian?
	end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
