class CreateEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.integer :id_book
      t.integer :id_client
      t.string :text
      t.integer :grade

      t.timestamps null: false
    end
  end
end
