class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :name
      t.integer :id_publisher
      t.integer :id_author
      t.string :synopsis
      t.integer :pages
      t.integer :releaseYear
      t.string :language

      t.timestamps null: false
    end
  end
end
