class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :id_copy
      t.integer :id_client
      t.integer :id_attendant
      t.datetime :startDate
      t.datetime :expectedDate
      t.datetime :deliveryDate
      t.string :loanStatus

      t.timestamps null: false
    end
  end
end
