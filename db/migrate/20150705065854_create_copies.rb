class CreateCopies < ActiveRecord::Migration
  def change
    create_table :copies do |t|
      t.integer :id_book
      t.string :copyStatus

      t.timestamps null: false
    end
  end
end
