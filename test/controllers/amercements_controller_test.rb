require 'test_helper'

class AmercementsControllerTest < ActionController::TestCase
  setup do
    @amercement = amercements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:amercements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create amercement" do
    assert_difference('Amercement.count') do
      post :create, amercement: { id_loan: @amercement.id_loan, price: @amercement.price }
    end

    assert_redirected_to amercement_path(assigns(:amercement))
  end

  test "should show amercement" do
    get :show, id: @amercement
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @amercement
    assert_response :success
  end

  test "should update amercement" do
    patch :update, id: @amercement, amercement: { id_loan: @amercement.id_loan, price: @amercement.price }
    assert_redirected_to amercement_path(assigns(:amercement))
  end

  test "should destroy amercement" do
    assert_difference('Amercement.count', -1) do
      delete :destroy, id: @amercement
    end

    assert_redirected_to amercements_path
  end
end
